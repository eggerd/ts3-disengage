# TS3-Disengage

[![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17639867%2Frepository%2Ftags&query=%24%5B0%5D.name&colorB=blue)](https://gitlab.com/eggerd/ts3-disengage/-/releases)
[![pipeline status](https://gitlab.com/eggerd/ts3-disengage/badges/master/pipeline.svg)](https://gitlab.com/eggerd/ts3-disengage/-/pipelines) 
[![quality gate](https://sonarcloud.io/api/project_badges/measure?project=ts3-disengage&metric=alert_status)](https://sonarcloud.io/dashboard?id=ts3-disengage) 
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=ts3-disengage&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=ts3-disengage)

Teamspeak 3 has a feature for automatically kicking users that are inactive for a configured amount of time. However, there is sadly no way to configure *when* this feature should be enabled - e.g. only if the server reaches its user limit. So this script acts as a replacement for Teamspeaks native feature, allowing you to specify when idling users should be kicked. Affected users will be notified with a customizable message, to let them know why they are being kicked.

In short:
- Kick idling users based on the amount of free slots remaining
- Notify idling users that they are being kicked to make room for other users
- Supports easy localization of notification & kick messages (currently only German & English are included)

## Requirements

- PHP 7.0 or higher
- Teamspeak Server 3.2.0 or higher
- Access to the Teamspeak 3 query over Telnet, with a query account that has at least [these permissions](https://gitlab.com/eggerd/ts3-disengage/-/wikis/home#teamspeak-3-query)
- Ability to schedule cronjobs
- [Composer](https://getcomposer.org/), to install dependencies

## Installation

1. Download the [latest version](https://gitlab.com/eggerd/ts3-disengage/-/releases)
1. Run `composer install` in the root directory of the project
1. Edit the [configuration](https://gitlab.com/eggerd/ts3-disengage/-/wikis/home#available-settings) files in `configs/` according to your needs
1. Upload all files to your server
1. [Set up a cronjob](https://gitlab.com/eggerd/ts3-disengage/-/wikis/home#cronjob) that executes the `index.php` every minute

> The script accesses the Teamspeak 3 query over Telnet. Therefore, traffic between the script and the TS3 query is not encrypted! Due to this, it is recommended that the script is executed on the same machine as the TS3 server itself.

## Documentation

A documentation with more details can be found in the [wiki](https://gitlab.com/eggerd/ts3-disengage/-/wikis) of the repository.

## Licensing

This software is available freely under the MIT License.  
Copyright (c) 2020 Dustin Eckhardt
