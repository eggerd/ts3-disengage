<?php

/**
 * Used to trigger a custom error message, if any TS3 query command fails. Every error returned by
 * class::ts3admin will be triggered as E_USER_NOTICE
 *
 * @param array $errors = array of error messages returned by class::ts3admin
 * @param string [$message] = the custom error message (ignored if empty)
 * @return void
 */
function trigger_query_error(array $errors, $message = '')
{
	if(!empty($message))
	{
		trigger_error($message, E_USER_WARNING);
	}

	foreach($errors as $e)
	{
		trigger_error('Query returned: '.$e, E_USER_NOTICE);
	}
}



/**
 * Prints a debug message, if debugging is enabled
 *
 * @param string $message
 * @return void
 */
function debug(string $message) {
	$linebreak = isset($_SERVER['HTTP_USER_AGENT']) ? '<br />' : "\n";
	if(DEBUG) { echo $message.$linebreak; }
}

?>