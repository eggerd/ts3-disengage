<?php

require_once('./vendor/autoload.php');
require_once('./configs/configure.php');
require_once('./includes/bugsnag.php');
require_once('./includes/functions/helper.php');
require_once('./includes/functions/data.php');

debug(date('Y-m-d H:i:s'));


// +++++ Connecting +++++

$TS3QUERY = new ts3admin(TS3_HOSTNAME, TS3_QUERY_PORT);
$ts3query_connect = $TS3QUERY->connect();
if(!$ts3query_connect['success'])
{
	trigger_query_error($ts3query_connect['errors']);
	throw new Exception('Failed to establish connection to the TS3 query!');
}

$ts3query_login = $TS3QUERY->login(TS3_QUERY_USERNAME, TS3_QUERY_PASSWORD);
if(!$ts3query_login['success'])
{
	trigger_query_error($ts3query_login['errors']);
	throw new Exception('Failed to authenticate query account!');
}

$ts3query_select = $TS3QUERY->selectServer(TS3_SERVER_PORT);
if(!$ts3query_select['success'])
{
	trigger_query_error($ts3query_select['errors']);
	throw new Exception('Failed to select virtual server!');
}

if($TS3QUERY->whoAmI()['data']['client_nickname'] !== TS3_NICKNAME)
{
	$ts3query_rename = $TS3QUERY->setName(TS3_NICKNAME);
	if(!$ts3query_rename['success'])
	{
		trigger_query_error($ts3query_rename['errors']);
		throw new Exception('Failed to rename query user!');
	}
}


// +++++ Guards +++++

$server = $TS3QUERY->serverInfo();
if(!$server['success'])
{
	trigger_query_error($server['errors']);
	throw new Exception('Failed to get server infos!');
}

$empty_slots = $server['data']['virtualserver_maxclients'] - $server['data']['virtualserver_clientsonline'];
if($empty_slots > TRIGGER_EMPTY_SLOTS) {
	debug('nothing to do');
	data_write(0);
	exit;
}

$data = data_read();
if ($data === 0) {
	$data = time();
	data_write($data);
}

if (time() - $data < GRACE_PERIOD) {
	debug('grace period');
	exit;
}



// +++++ Notify & Kick Clients +++++

$clientlist = $TS3QUERY->clientList('-country -groups -times');
if(!$clientlist['success'])
{
	trigger_query_error($clientlist['errors']);
	throw new Exception('Failed to get list of currently connected clients!');
}

foreach($clientlist['data'] as $client) {
	if($client['client_type'] != "0") continue; // skip if it's a query user

	$max_idle_time = -1;
	$groups = explode(',', $client['client_servergroups']);
	foreach($groups as $group) {
		if(isset(IDLE_TIMES[$group]) && IDLE_TIMES[$group] > $max_idle_time) {
			$max_idle_time = IDLE_TIMES[$group];
		}
	}

	if($max_idle_time < 0) continue; // skip if no max idle time was found

	$idle_time = round($client['client_idle_time'] / 1000);
	if($idle_time < $max_idle_time) continue;

	debug('kicking client '.$client['client_database_id'].' (groups: '.$client['client_servergroups'].' / idle: '.$idle_time.' / max: '.$max_idle_time.')');

	$language = $client['client_country'];
	$message = isset(MESSAGES[$language]) ? MESSAGES[$language] : MESSAGES[DEFAULT_LANGUAGE];
	$reason = isset(REASONS[$language]) ? REASONS[$language] : REASONS[DEFAULT_LANGUAGE];

	$notify = $TS3QUERY->sendMessage(1, $client['clid'], '[b][color=red]'.$message.'[/color][/b]');
	if(!$notify['success']) {
		trigger_query_error($notify['errors'], 'Failed to notify user with database ID #'.$client['client_database_id']);
	}

	$kick = $TS3QUERY->clientKick($client['clid'], 'server', $reason);
	if(!$kick['success']) {
		trigger_query_error($notify['errors'], 'Failed to kick user with database ID #'.$client['client_database_id']);
	}
}

debug('jobs done');

?>
