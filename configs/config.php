<?php

config::set('VERSION', '1.1.0');


// ***** Teamspeak Query *****

// The FQDN or IP of the TS3 server
config::set('TS3_HOSTNAME', '127.0.0.1'); // :string

// The port number of the TS3 server itself
config::set('TS3_SERVER_PORT', 9987); // :integer

// The port number of the TS3 query
config::set('TS3_QUERY_PORT', 30033); // :integer

// The username of the query account (see documentation for a list of required permissions)
config::set(config::secret('TS3_QUERY_USERNAME')); // :string

// The password for the query account
config::set(config::secret('TS3_QUERY_PASSWORD')); // :string

// The username that should be used when connecting to the TS3 query & poking users
config::set('TS3_NICKNAME', 'Server [Disengage]'); // :string



// ***** General *****

// Set the maximum idle time in seconds per group (syntax: group => time)
config::set('IDLE_TIMES', [
  8 => 60 * 20, // bronze
  7 => 60 * 40, // silver
  9 => 60 * 60, // gold
  10 => 60 * 90, // platinum
  11 => 60 * 90 // vip
]); // :array

// Kick absent clients if only this many empty slots remain
config::set('TRIGGER_EMPTY_SLOTS', 0); // :integer

// Time in seconds to wait after reaching TRIGGER_EMPTY_SLOTS, before the script actually starts
// kicking any absent clients. Used to prevent situations were the threshold gets reached only for
// a short time, because someone joined and then left again (for example). Would be unnecessary to
// kick any clients in those situations
config::set('GRACE_PERIOD', 60 * 5);

// Two letter country code that exists in the MESSAGES array (case sensitive!) and should be used
// as the default language, in case a users language could not be determined or isn't available
config::set('DEFAULT_LANGUAGE', 'EN'); // :string

// An array that specifies the client notification message in different languages.
// Syntax: language => message
// Where "language" is a two letter country code (in all caps) used by TS3 in the "client_country"
// field (e.g. "EN" for English)
config::set('MESSAGES', [
  'EN' => '\nSorry! You were kicked automatically to make room for other users, because the server has reached its user limit and you have been absent for some time – you may try to reconnect when you\'re back.',
  'DE' => '\nSorry! Du wurdest automatisch gekickt, um Platz für andere Benutzer zu machen, da der Server sein Benutzerlimit erreicht hat und du einige Zeit abwesend warst – du kannst versuchen dich neu zu verbinden, wenn du wieder da bist.'
]); // :array

// An array that specifies the kick reason in different languages.
// See MESSAGES for details about the syntax, but note that reasons are limited to 80 characters!
config::set('REASONS', [
  'EN' => 'server full – idle time exceeded – making room for others – sorry!',
  'DE' => 'Server voll – AFK-Zeit überschritten – Platz machen für andere – Sorry!'
]); // :array



/***** Error Tracking *****/

// API key for the Bugsnag project (leave empty to disable tracking)
config::set('BUGSNAG_API_KEY', ''); // :string

// List of environment names that will be tracked by Bugsnag
config::set('BUGSNAG_TRACK_ENVIRONMENTS', ['prod', 'test']); // :array

// Whether debug messages should be printed
config::set('DEBUG', false); // :boolean

?>
