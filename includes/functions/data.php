<?php

/**
 * Reads the stringified data from the data file , decodes it and then returns the data
 *
 * @return integer = the decoded contents of the data file
 */
function data_read()
{
	$file_path = realpath(dirname(__FILE__).'/../../').'/data/'.'timestamp.json';
	if(!is_readable($file_path))
	{
		data_write(0);
		return 0;
	}

	$file = fopen($file_path, 'r');
	$data = json_decode(fread($file, filesize($file_path) + 1), true);
	fclose($file);

	return $data;
}



/**
 * Stringifies the passed data and writes it into the data file
 *
 * @param integer $data = the data that should be written into the data file
 * @return void
 *
 * @throws Exception Failed to create data directory!
 * @throws Exception Failed to write into data file!
 */
function data_write(int $data)
{
	$data_path = realpath(dirname(__FILE__).'/../../').'/data/';
	if(!is_dir($data_path) && !mkdir($data_path, 0775))
	{
		throw new Exception('Failed to create data directory!');
	}

	$file = fopen($data_path.'timestamp.json', 'w+'); // open file; create if not existing
	if(fwrite($file, json_encode($data)) === false)
	{
		throw new Exception('Failed to write into data file!');
	}
}

?>
