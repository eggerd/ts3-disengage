1.1.0 - 02.03.2022
------------------
- Clients are now kicked directly by the script, instead of Teamspeaks idle timeout feature
- Added an option for a grace period, before actually kicking clients after hitting the threshold
- Added an option to customize kick reasons


1.0.0 - 21.05.2020
------------------
Initial Release
